"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var create = Object.create;
var setPrototypeOf = Object.setPrototypeOf;
var assign = Object.assign;

function APIError(msg, code) {
  if (!(this instanceof APIError)) return new APIError(msg, code);
  var tmp = Error(msg);
  this.message = tmp.message;
  this.stack = tmp.stack;
  this.code = code;
}

function CustomError(msg) {
  if (!(this instanceof CustomError)) return new CustomError(msg);
  var tmp = Error(msg);
  this.message = tmp.message;
  this.stack = tmp.stack;
}

CustomError.prototype = assign(create(Error.prototype), {
  name: CustomError.name,
  constructor: CustomError,
  toSerializable: function toSerializable() {
    return this.constructor.toSerializable(this);
  }
});

assign(CustomError, {
  toSerializable: function toSerializable(e) {
    var error = this(e.message);
    error.error = true;
    error.code = e.code;
    error.name = error.name;
    error.message = e.message;
    delete error.stack;
    return error;
  }
});

setPrototypeOf(APIError, CustomError);

APIError.prototype = assign(create(CustomError.prototype), {
  name: APIError.name,
  constructor: APIError
});

var errors = module.exports = {
  APIError: APIError,
  serializableFromError: serializableFromError,
  errorFromCode: errorFromCode
};

function serializableFromError(e, name) {
  if (!name) name = APIError.name;
  return errors[name].toSerializable(e);
}

var errorCodes = {
  '1': 'Video not found'
};
function errorFromCode(code) {
  var err = errorCodes[code];
  if (!err) throw Error('error code not found');
  if ((typeof err === 'undefined' ? 'undefined' : _typeof(err)) === 'object') return errors[err.name](err.message, code);
  return APIError(err, code);
}