"use strict";

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _views = require('./views');

var _views2 = _interopRequireDefault(_views);

var _errors = require('./errors');

var _errors2 = _interopRequireDefault(_errors);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _path = require('path');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var assign = Object.assign;


function Exstream(cfg) {
  if (!(this instanceof Exstream)) return new Exstream(cfg);
  assign(_config2.default, cfg);
  this._app = (0, _express2.default)();
  this.config = _config2.default;
  this._errorFactory = _errors2.default;
  (0, _views2.default)(this);
  this._app.use(_bodyParser2.default.urlencoded({
    extended: false
  }));
  this._app.use(_bodyParser2.default.json());
  require((0, _path.join)(__dirname, '..', 'routes'))(this);
  this._app.listen(_config2.default.port);
}

Exstream.prototype = {
  getConfig: function getConfig() {
    return this.config;
  },
  getErrorFactory: function getErrorFactory() {
    return this._errorFactory;
  }
};

module.exports = Exstream;