"use strict";

var _expressHandlebars = require('express-handlebars');

var _expressHandlebars2 = _interopRequireDefault(_expressHandlebars);

var _path = require('path');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (server) {
  var app = server._app;

  app.engine('.hbs', (0, _expressHandlebars2.default)({
    extname: '.hbs'
  }));
  app.enable('view cache');
  app.set('views', (0, _path.join)(__dirname, '..', '..', 'views'));
  app.set('view engine', '.hbs');
  return server;
};