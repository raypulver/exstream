"use strict";

var _util = require("../lib/util");

module.exports = function (server) {
  (0, _util.loadRoutesSync)(server._app, __filename);
  (0, _util.loadChildrenSync)(server, server._app, __filename);
};