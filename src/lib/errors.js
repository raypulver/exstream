"use strict";

const { create, setPrototypeOf, assign } = Object;
function APIError(msg, code) {
  if (!(this instanceof APIError)) return new APIError(msg, code);
  let tmp = Error(msg);
  this.message = tmp.message;
  this.stack = tmp.stack;
  this.code = code;
}

function CustomError(msg) {
  if (!(this instanceof CustomError)) return new CustomError(msg);
  let tmp = Error(msg);
  this.message = tmp.message;
  this.stack = tmp.stack;
}

CustomError.prototype = assign(create(Error.prototype), {
  name: CustomError.name,
  constructor: CustomError,
  toSerializable() {
    return this.constructor.toSerializable(this);
  }
});

assign(CustomError, {
  toSerializable(e) {
    let error = this(e.message);
    error.error = true;
    error.code = e.code;
    error.name = error.name;
    error.message = e.message;
    delete error.stack;
    return error;
  }
});

setPrototypeOf(APIError, CustomError);

APIError.prototype = assign(create(CustomError.prototype), {
  name: APIError.name,
  constructor: APIError
});

let errors = module.exports = {
  APIError,
  serializableFromError,
  errorFromCode
};

function serializableFromError(e, name) {
  if (!name) name = APIError.name;
  return errors[name].toSerializable(e);
}

const errorCodes = {
  '1': 'Video not found'
};
function errorFromCode(code) {
  let err = errorCodes[code];
  if (!err) throw Error('error code not found');
  if (typeof err === 'object')
    return errors[err.name](err.message, code);
  return APIError(err, code);
}
