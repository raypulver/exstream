"use strict";

import config from '../../config';
import express from 'express';
import setupViews from './views';
import errorFactory from './errors';
import bodyParser from 'body-parser';
import { join } from 'path';

const { assign } = Object;

function Exstream(cfg) {
  if (!(this instanceof Exstream)) return new Exstream(cfg);
  assign(config, cfg);
  this._app = express();
  this.config = config;
  this._errorFactory = errorFactory;
  setupViews(this);
  this._app.use(bodyParser.urlencoded({
    extended: false
  }));
  this._app.use(bodyParser.json());
  require(join(__dirname, '..', 'routes'))(this);
  this._app.listen(config.port);
}

Exstream.prototype = {
  getConfig() { return this.config; },
  getErrorFactory() { return this._errorFactory; }
};

module.exports = Exstream;
