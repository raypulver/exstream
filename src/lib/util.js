/** @module dist/lib/util
 *  @description Utility functions for application
 */

"use strict";

import clone from 'clone';
import chalk from 'chalk';
import { forOwn, difference } from 'lodash'; 
import { Router } from 'express';
import { readdirSync, lstatSync } from 'fs';
import { join, parse } from 'path';
import config from '../../config';
import { map, mapSeries } from 'async';
import { format } from 'url';

const { 
  create,
  getOwnPropertyDescriptor,
  getPrototypeOf,
  setPrototypeOf
} = Object;


/** @description Map object to a new object, similar to Array#map
 * @param {Object} object The object to map
 * @param {Function} cb The callback function
 * @returns {Object} New object
 */

function mapOwn(object, cb) {
  let retval = create(getPrototypeOf(object));
  forOwn(object, function (val, key) {
    retval[key] = cb(val, key, object);
  });
  return retval;
}

/** @description Inject a prototype between the object and its current object, setting the current prototype to be the prototype of the injected prototype
 * @param {Object} target The object to modify
 * @param {Object} proto The prototype to inject
 * @returns {Object} returns its first argument
 */

function injectPrototype(target, proto) {
  return Object.setPrototypeOf(target, create(getPrototypeOf(target), mapOwn(proto, (val, key, obj) => {
    return getOwnPropertyDescriptor(obj, key);
  })));
}

const RouterProto = {
  load() {
    loadRoutesSync(this, this._filename);
    loadChildrenSync(this.getServer(), this, this._filename);
    return this;
  },
  getServer() {
    return this._server;
  },
  getErrorFactory() {
    return this.getServer().getErrorFactory();
  },
  getServerConfig() {
    return this.getServer().config;
  },
  getUtil() {
    return this._util;
  }
};

/** @description Create a router that keeps a reference to the main server and loads its routes and child routers from the directory structure
 * @param {module:dist/lib/server~Server} server The server object
 * @param {string} dir The directory containing the routes (usually __dirname)
 * @returns {external:Router} The router object
 * @see {module:dist/lib/util~loadRoutesSync}
 * @see {module:dist/lib/util~loadChildrenSync}
 */

function makeRouter(server, file) {
  let retval = Router();
  retval._server = server;
  retval._util = require(__filename);
  injectPrototype(retval, RouterProto);
  retval._filename = file;
  return retval;
}

/** @description Load route files from directory and attach them to router, routes are passed the instance of the router as an argument
 * @param {external:Router} router The router instance
 * @param {string} file The filepath of the router, passed in from __filename
 * @returns {undefined}
 */

function loadRoutesSync(router, file) {
  let parts = parse(file);
  const { base, dir } = parts;
  readdirSyncExclude(dir, base).forEach((v) => {
    let path = join(dir, v);
    parts = parse(v);
    if (!lstatSync(path).isDirectory() && parts.ext === '.js') require(path)(router);
  });
}

/** @description Load child routers from subdirectories and attach them to router, routers are passed the instance of the server as an argument
 * @param {external:Router} router The router instance
 * @param {string} dir The directory to load from
 * @returns {undefined}
 */

function loadChildrenSync(server, router, file) {
  let parts = parse(file);
  const { base, dir } = parts;
  readdirSyncExclude(dir, base).filter((v) => {
    return lstatSync(join(dir, v)).isDirectory();
  }).forEach((v) => {
    router.use(join('/', v), require(join(dir, v))(server));
  });
}

/** @description Perform fs.readdirSync but exclude a file or list of files from results
 * @param {string} path The filepath to stat
 * @param {string|Array.<string>} exclude The file to exclude, or array of files
 * @returns {Array.<string>} The list of files in the directory
 */

function readdirSyncExclude(path, exclude) {
  if (typeof exclude === 'string') exclude = [ exclude ];
  return difference(readdirSync(path), exclude);
}

const nullifier = {
  open: '',
  close: ''
};

/**
 * @description
 * Explicitly disables the action of chalk, preventing color from being added
 * @returns {boolean} Returns false if chalk was already disabled
 */

function neutralizeColor() {
  if (!neutralizeColor.cache) {
    neutralizeColor.cache = clone(chalk.styles);
    Object.keys(chalk.styles).forEach(function(v) {
      Object.assign(chalk.styles[v], nullifier);
    });
    return true;
  }
  return false;
}

/**
 * @description
 * Re-enable chalk if it was previously disabled
 * @returns {boolean} Returns false if chalk was already enabled
 */

function reenableColor() {
  if (!neutralizeColor.cache) return false;
  chalk.styles = neutralizeColor.cache;
  delete neutralizeColor.cache;
  return true;
}

const colorRe = /\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]/g;

/**
 * @description Removes color codes from string using regular expression
 * @param {string} str The string to remove color codes from
 * @returns {string} The string with color codes removed
 */

function stripColor(str) {
  return str.replace(colorRe, '');
}

/**
 * @description
 * Splices out an event handler from an EventEmitter instance that does not implement EventEmitter#off
 * @returns {void}
 */

function spliceHandler(sock, evt, fn) {
  if (Array.isArray(sock._events[evt])) {
    let idx = sock._events[evt].indexOf(fn);
    if (!~idx) return;
    sock._events[evt].splice(idx, 1);
    return;
  }
  if (typeof sock._events[evt] === 'function') delete sock._events[evt];
}

/** @description Extracts the name from the filepath, without extension
 * @param {string} path
 * @returns {string} The name
 * @example
 * filepathName('/etc/ssl/openssl.cnf')
 * // 'openssl'
 */

function filepathName(path) {
  return parse(path).name;
}

/** @description Extracts the base from the filepath, without directory
 * @param {string} path
 * @returns {string} The base
 * @example
 * filepathBase('/etc/ssl/openssl.cnf');
 * // 'openssl.cnf'
 */

function filepathBase(path) {
  return parse(path).base;
}

function endpoint(filename) {
  return join('/', filepathName(filename));
}

module.exports = {
  endpoint,
  filepathBase,
  filepathName,
  makeRouter,
  loadRoutesSync,
  loadChildrenSync,
  injectPrototype,
  mapOwn,
  readdirSyncExclude,
  neutralizeColor,
  reenableColor,
  stripColor,
  spliceHandler,
  mapSeries(arr, fn) {
    return new Promise((resolve, reject) => {
      mapSeries(arr, fn, (err, results) => {
        if (err) return reject(err);
        resolve(results);
      });
    });
  }
};
