"use strict";

import exphbs from 'express-handlebars';
import { join } from 'path';

module.exports = (server) => {
  let { _app: app } = server;
  app.engine('.hbs', exphbs({
    extname: '.hbs'
  }));
  app.enable('view cache');
  app.set('views', join(__dirname, '..', '..', 'views'));
  app.set('view engine', '.hbs');
  return server;
};
