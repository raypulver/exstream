"use strict";

import { loadRoutesSync, loadChildrenSync } from '../lib/util';

module.exports = function(server) {
  loadRoutesSync(server._app, __filename);
  loadChildrenSync(server, server._app, __filename);
};
