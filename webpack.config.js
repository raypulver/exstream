"use strict";

const path = require('path'),
  webpack = require('webpack'),
  ClosureCompilerPlugin = require('webpack-closure-compiler'),
  assetPath = path.join(__dirname, 'assets'),
  publicPath = path.join(__dirname, 'public');

module.exports = {
  entry: path.join(assetPath, 'main.js'),
  output: {
    path: publicPath,
    filename: 'bundle.min.js'
  },
  devtool: 'source-map',
  plugins: [
    new ClosureCompilerPlugin({
      compiler: {
        language_in: 'ECMASCRIPT6',
        language_out: 'ECMASCRIPT5'
      },
      concurrency: 3
    })
  ],
  module: {
    loaders: [{
      test: /(?:\.js$)/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        presets: ['es2015']
      }
    }, {
      test: /\.css$/,
      loader: 'style!css'
    }, {
      test: /\.less$/,
      loader: 'style!css!less',
    }, {
      test: /\.gif/,
      loader: 'url?limit=10000&mimetype=image/gif'
    }, {
      test: /\.jpe?g/i,
      loader: 'url?limit=340000&mimetype=image/jpg'
    }, {
      test: /\.png/i,
      loader: 'url?limit=340000&mimetype=image/png'
    }, {
      test: /\.woff|woff2$/,
      loader: 'url?limit=10000&mimetype=application/font-woff'
    }, {
      test: /(?:\.(?:ttf|eot|svg))(\?.*$|$)/,
      loader: 'file'
    }, {
      test: /\.json$/,
      loader: 'json'
    }]
  }
};
